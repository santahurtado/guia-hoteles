$(function(){
        $("[data-toggle='tooltip']").tooltip();
        $("[data-toggle='popover']").popover();
        $('.carousel').carousel({
          interval: 2000
        });
        $('#contact').on('show.bs.modal', function (e) {
          console.log('El modal se está mostrando');

          $('#contactBtn').removeClass('btn-outline-success');
          $('#contactBtn').addClass('btn-primary');
          $('#contactBtn').prop('disabled', true);

        });
        $('#contact').on('shown.bs.modal', function (e) {
          console.log('El modal se mostró');
        });
        $('#contact').on('hide.bs.modal', function (e) {
          console.log('El modal se oculta');
        });
        $('#contact').on('hidden.bs.modal', function (e) {
          console.log('El modal se ocultó');
          $('#contactBtn').prop('disabled', false);
          $('#contactBtn').removeClass('btn-primary');
          $('#contactBtn').addClass('btn-outline-success');
        });
      });